﻿using System;

class Program
{
    static void Main()
    {
        string[] fructi = { "Апельсины", "Бананы", "Абрикосы" };

        
        Console.WriteLine("Исходный массив:");
        foreach (string fruit in fructi)
        {
            Console.WriteLine(fruit);
        }
        string[] reverseFruits = new string[fructi.Length];
        for (int i = 0; i < fructi.Length; i++)
        {
            reverseFruits[i] = fructi[fructi.Length - 1 - i];
        }

        Console.WriteLine("\nОбратный массив:");
        foreach (string fruit in reverseFruits)
        {
            Console.WriteLine(fruit);
        }
    }
}